/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   newsimpletest.cpp
 * Author: abdullah
 *
 * Created on September 29, 2018, 11:34 AM
 */

#include <stdlib.h>
#include <iostream>
#include "/home/abdullah/NetBeansProjects/Task1_new/main.cpp"
/*
 * Simple C++ Test Suite
 */

student::student();

void testStudent() {
    student _student();
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testStudent (newsimpletest) message=error message sample" << std::endl;
    }
}

student::student(string a, string b, string c, string d, string e, string f, string g, string h, string i, string j, string k, string l);

void testStudent2() {
    string a;
    string b;
    string c;
    string d;
    string e;
    string f;
    string g;
    string h;
    string i;
    string j;
    string k;
    string l;
    student _student(a, b, c, d, e, f, g, h, i, j, k, l);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testStudent2 (newsimpletest) message=error message sample" << std::endl;
    }
}

float student::get_aggregate();

void testGet_aggregate() {
    student _student;
    float result = _student.get_aggregate();
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testGet_aggregate (newsimpletest) message=error message sample" << std::endl;
    }
}

process::process(string a);

void testProcess() {
    string a;
    process _process(a);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testProcess (newsimpletest) message=error message sample" << std::endl;
    }
}

void process::file_creator();

void testFile_creator() {
    process _process;
    _process.file_creator();
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testFile_creator (newsimpletest) message=error message sample" << std::endl;
    }
}

vector<student> process::collaborate();

void testCollaborate() {
    process _process;
    vector<student> result = _process.collaborate();
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testCollaborate (newsimpletest) message=error message sample" << std::endl;
    }
}

vector<float> process::get_aggregate_vector(vector<student> stu);

void testGet_aggregate_vector() {
    vector<student> stu;
    process _process;
    vector<float> result = _process.get_aggregate_vector(stu);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testGet_aggregate_vector (newsimpletest) message=error message sample" << std::endl;
    }
}

vector<float> process::sort_vector(vector<float> stu);

void testSort_vector() {
    vector<float> stu;
    process _process;
    vector<float> result = _process.sort_vector(stu);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testSort_vector (newsimpletest) message=error message sample" << std::endl;
    }
}

void process::write_csv(int value, string first_name, string second_name, string cms);

void testWrite_csv() {
    int value;
    string first_name;
    string second_name;
    string cms;
    process _process;
    _process.write_csv(value, first_name, second_name, cms);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testWrite_csv (newsimpletest) message=error message sample" << std::endl;
    }
}

void process::writing_grades(float a, float b, float c, float d, vector<student> stu);

void testWriting_grades() {
    float a;
    float b;
    float c;
    float d;
    vector<student> stu;
    process _process;
    _process.writing_grades(a, b, c, d, stu);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testWriting_grades (newsimpletest) message=error message sample" << std::endl;
    }
}

float process::get_average(vector<student> stu);

void testGet_average() {
    vector<student> stu;
    process _process;
    float result = _process.get_average(stu);
    if (true /*check result*/) {
        std::cout << "%TEST_FAILED% time=0 testname=testGet_average (newsimpletest) message=error message sample" << std::endl;
    }
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% newsimpletest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% testStudent (newsimpletest)" << std::endl;
    testStudent();
    std::cout << "%TEST_FINISHED% time=0 testStudent (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testStudent2 (newsimpletest)" << std::endl;
    testStudent2();
    std::cout << "%TEST_FINISHED% time=0 testStudent2 (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testGet_aggregate (newsimpletest)" << std::endl;
    testGet_aggregate();
    std::cout << "%TEST_FINISHED% time=0 testGet_aggregate (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testProcess (newsimpletest)" << std::endl;
    testProcess();
    std::cout << "%TEST_FINISHED% time=0 testProcess (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testFile_creator (newsimpletest)" << std::endl;
    testFile_creator();
    std::cout << "%TEST_FINISHED% time=0 testFile_creator (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testCollaborate (newsimpletest)" << std::endl;
    testCollaborate();
    std::cout << "%TEST_FINISHED% time=0 testCollaborate (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testGet_aggregate_vector (newsimpletest)" << std::endl;
    testGet_aggregate_vector();
    std::cout << "%TEST_FINISHED% time=0 testGet_aggregate_vector (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testSort_vector (newsimpletest)" << std::endl;
    testSort_vector();
    std::cout << "%TEST_FINISHED% time=0 testSort_vector (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testWrite_csv (newsimpletest)" << std::endl;
    testWrite_csv();
    std::cout << "%TEST_FINISHED% time=0 testWrite_csv (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testWriting_grades (newsimpletest)" << std::endl;
    testWriting_grades();
    std::cout << "%TEST_FINISHED% time=0 testWriting_grades (newsimpletest)" << std::endl;

    std::cout << "%TEST_STARTED% testGet_average (newsimpletest)" << std::endl;
    testGet_average();
    std::cout << "%TEST_FINISHED% time=0 testGet_average (newsimpletest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

