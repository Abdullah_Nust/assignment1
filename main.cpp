#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>    


using namespace std;
/********************************************//**
 *  ... Student class, where all attributes of students are saved
 ***********************************************/

class student {
public:
    /*!
 * ...... Student class attributes
 */
    string first_name;
    string last_name;
    string cms;
    string A1_marks;
    string A2_marks;
    string A3_marks;
    string Q1_marks;
    string Q2_marks;
    string Q3_marks;
    string OHT1_marks;
    string OHT2_marks;
    string ESE_marks;

    /*!
 * ...... Student class constructor
 */
    student(){
        
    }
    student(string a, string b, string c, string d, string e, string f, string g, string h, string i, string j, string k, string l){
        
        cms = a;
        first_name = b;
        last_name = c;
        Q1_marks = d;
        A1_marks = e;
        A2_marks = f;
        A3_marks = g;
        Q2_marks = h;
        Q3_marks = i;
        OHT1_marks = j;
        OHT2_marks = k;
        ESE_marks = l;
        
    }
    /*!
 * ...... Method to get aggregate of the given object of student
 */
    float get_aggregate(){
        float aggregate = ((atof(A1_marks.c_str())+atof(A2_marks.c_str())+atof(A3_marks.c_str()))/30)*10 + ((atof(Q1_marks.c_str())+atof(Q2_marks.c_str())+atof(Q3_marks.c_str()))/30)*10 +  ((atof(OHT1_marks.c_str())+atof(OHT2_marks.c_str()))/100)*40 +  ((atof(ESE_marks.c_str()))/100)*40;
        return aggregate;
    }
    
};
/********************************************//**
 *  ... Process class, where all processing on Student class is done
 ***********************************************/
  class process {
    public:
        /*!
 * ...... Process class attributes
 */
        ifstream file;
        ofstream file1, file2, file3, file4, file5, file6, file7, file8;
        string file_name ;
        string sub_String ;
        string A_csv;
        string B_csv;
        string BB_csv;
        string C_csv;
        string CC_csv;
        string D_csv;
        string DD_csv;
        string F_csv;
        
        
            /*!
 * ...... Process class constructor
 */
        process(string a){
            file_name = a;
            sub_String = file_name;
        }
        
        string raw;
        
        vector<student> student_array;
        
        /*!
 * ...... Process class method void which is used to open csv files to write data on.
 */
        
        void file_creator(){
            A_csv = sub_String+"-A.csv";
            B_csv = sub_String+"-B.csv";
            BB_csv = sub_String+"-B+.csv";
            C_csv = sub_String+"-C.csv";
            CC_csv = sub_String+"-C+.csv";
            D_csv = sub_String+"-D.csv";
            DD_csv = sub_String + "-D+.csv";
            F_csv = sub_String+"-F.csv";
            
            file1.open(A_csv.c_str());   file2.open(BB_csv.c_str()); file3.open(B_csv.c_str()); file4.open(CC_csv.c_str()); file5.open(C_csv.c_str()); file6.open(DD_csv.c_str()); file7.open(D_csv.c_str()); file8.open(F_csv.c_str());
            
            
        }
        
        
        /*!
 * ...... Process class method collaborate to return vector of type students having all objects of studetns
 */
        vector<student> collaborate(){
            file.open( file_name.c_str());
            int ans = 0;
            while (file.good()){
                string a, b, c, d, e, f,g, h, i, j, k, l, m, o;
                if (ans >1){
                    getline(file,a ,',');
                    getline(file,b,',');
                    getline(file,c,',');
                    getline(file,d,',');
                    getline(file,e,',');
                    getline(file,f,',');
                    getline(file,g,',');
                    getline(file,h,',');
                    getline(file,i,',');
                    getline(file,j,',');
                    getline(file,k,',');
                    getline(file,l,'\n');
                    student std_obj(a, b, c, d, e, f, g, h, i, j, k, l);
                    student_array.push_back(std_obj);
                }
                else {
                    getline(file,raw ,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,',');
                    getline(file,raw,'\n');
                    ans +=1;
                }
                        
            }     
            return student_array;
        }        
        /*!
 * ...... Process class method get_aggregate_vector to get vector of all aggregates of student in a single vector
 */
        vector<float> get_aggregate_vector(vector<student> stu){
            vector<float> aggregate_vector;
            for(int i = 2; i < stu.size(); i++ ){
                aggregate_vector.push_back(stu[i].get_aggregate());
            }
            return aggregate_vector;
        }
        /*!
 * ...... Process class method to sort vector and then get the range of grades
 */
             vector<float>  sort_vector(vector<float> stu){
                vector<float>  returning;
                vector<float> vector_aggregate = stu;
                sort(vector_aggregate.begin(),vector_aggregate.begin() + vector_aggregate.size());

                returning.push_back(vector_aggregate[0]);
                 returning.push_back(vector_aggregate[9]);
                 returning.push_back(vector_aggregate[stu.size()-10]);
                 returning.push_back(vector_aggregate[stu.size()-1]);
            
            
            
            
            return returning;
        }
             /*!
 * ...... Process class method to create csv files
 */
             void write_csv(int value, string first_name, string second_name, string cms){
                 if (value == 1)
                 file1<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 2)
                 file2<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 3)
                 file3<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 4)
                 file4<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 5)
                 file5<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 6)
                 file6<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 7)
                 file7<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
                 if (value == 8)
                 file8<< first_name<<"  "<<second_name<<"  "<<cms<<endl;
                 
             }
        /*!
 * ...... Process class method to set the attributes of class grades
 */
             void writing_grades(float a, float b, float c, float d, vector<student> stu){
                 // A grade
                 cout<<"Writing grades"<<endl;
                 sub_String.erase(file_name.size()-4,file_name.size());
                 file_creator();
                 for (int i = 0; i< stu.size(); i++){
                     if (stu[i].get_aggregate() > c){       //A
                         write_csv( 1,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());}
                     
                     if (stu[i].get_aggregate()<b){       //F
                        write_csv( 8,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
                     }
                     
                     if (stu[i].get_aggregate() > process::get_average(stu)){
                         if (72>stu[i].get_aggregate() and  stu[i].get_aggregate()>process::get_average(stu))       // B
                             write_csv( 3,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
                         if (c>stu[i].get_aggregate() and stu[i].get_aggregate()> 72)                                                 // B+
                             write_csv( 2,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
                     }
                     
                    if (stu[i].get_aggregate() <process::get_average(stu)  and b<stu[i].get_aggregate()  ){
                        
                        if (stu[i].get_aggregate() > 60)                                 // C+
                            write_csv( 4,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
                        
                        if (  60>stu[i].get_aggregate() and stu[i].get_aggregate() > 56)                   // C
                            write_csv( 5,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());

                        if ( 56>stu[i].get_aggregate() and stu[i].get_aggregate() > 52)                     // D+
                             write_csv( 6,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
                        
                        if (stu[i].get_aggregate() < 52 and stu[i].get_aggregate() > b)
                             write_csv( 7,stu[i].first_name.c_str(),stu[i].last_name.c_str(), stu[i].cms.c_str());
              
                     }
                 }
             }
             
             /*!
 * ...... Process class method to get average of all students
 */
       float get_average(vector<student> stu){
            
            float average = 0;
            
            for (int i = 2; i < stu.size(); i++){
                average += stu[i].get_aggregate();
            }   
            average = average/(stu.size()-2);
            return average;   
        }
    };
    
/********************************************//**
 *  ... Main method
 ***********************************************/
int main(int argc, char** argv) {
    
    
    /*!
 * ...... filename to be imported from terminal command
 */
    string file_name = argv[1];
    
    
    /*!
 * ...... Process class instantiation
 */
    process pro(file_name);
    vector<student> st = pro.collaborate();
    cout<<"Program running"<<endl;
    vector<float> aggregate = pro.get_aggregate_vector(st);
    float a, b, c, d;
   vector<float> returning = pro.sort_vector(aggregate);
   a = returning[0];b = returning[1];c = returning[2];d = returning[3];
   /*!
 * ...... After all processing writing grades
 */
    pro.writing_grades(a, b, c, d, st);
}
